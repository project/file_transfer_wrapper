CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Recommended Modules


INTRODUCTION
------------

Developers Only
This module will help you to hide all the complexity of Remote File Transfer
operations by exposing upload/download methods and configuration page. You can
create multiple connections to work with multiple sFTP servers. No need to worry
about error handling, error messages would be saved to your watchdog table.

 * Library supported: Phpseclib
 * Protocol supported: sFTP
 * Authentication supported: Password
 * Private key (.ppk) 


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information. Example :

 * Place the entire module directory into your Drupal directory:
   sites/all/modules/
 * Enable the module by navigating to:
   administer > build > modules
 * Click the "Save configuration" button at the bottom to commit your
   changes.

 *Note: It is assumed that you have Drupal up and running.  Be sure to
  check the Drupal web site if you need assistance.


CONFIGURATION
-------------

The configuration page for this module is at:
(admin/config/file_transfer_connections)

 *  Now you need to call appropriate methods whenever required from your code as
    below --

    try {
	// This will create a connection to the specified remote file server.
	// 'qa-portal' is the custom connection name that you need to add to the above
	// configuration page to identify a particular remote file server. Also it will
	// switch to the remote directory that is specified in the above configuration.
	$classObj = ftw\ FtwConnectionFactory::build('qa-portal');

	// Below are the methods that you can use as per your requirement.

	// Create Directory with the name specified.
	$classObj - > createDirectory('test2');

	// Download file based on the prefix defined on unless specified any
	// argument.
	$classObj - > downloadFile(); // Or you can specify remote file name as below
	$classObj - > downloadFile('remotefile.txt');

	// Upload a local file to the remote file server.
	$classObj - > uploadFile('example_1.txt');
    } catch (Exception $e) {
	drupal_set_message($e - > getMessage(), 'error');
    }

 * This also provides Test link for each connection which performs remote file
   server connection test step by step as below --
    - Create remote directory.
	- Create file to the above created remote directory.
	- Download above created file to the local directory as specified by the
	  configuration.
	- Recursively delete(all contents in this directory) remote directory.


RECOMMENDED MODULES
-------------------

 * Composer Manager : (https://www.drupal.org/project/composer_manager)
 * Encrypt : (https://www.drupal.org/project/encrypt)
