<?php

namespace ftw;

use Exception as Exception;
use Datetime as DateTime;
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;

/**
 * This class contains public methods for sFTP operations.
 */
class FtwPhpseclibSftp {

  /**
   * Authentication type.
   *
   * @var mixed
   */
  private $authType;

  /**
   * Host name.
   *
   * @var mixed
   */
  private $host;

  /**
   * Port, default is 22 for sftp.
   *
   * @var mixed
   */
  private $port;

  /**
   * Remote directory path.
   *
   * @var mixed
   */
  private $remoteDir;

  /**
   * Local directory path.
   *
   * @var mixed
   */
  private $localDir;

  /**
   * Remote file prefix.
   *
   * @var mixed
   */
  private $remoteFilePrefix;

  /**
   * Remote file download type in case of multiple file detected.
   *
   * @var mixed
   */
  private $downloadType;

  /**
   * Remote resource object.
   *
   * @var mixed
   */
  private $remoteResource = NULL;

  /**
   * Remote file list.
   *
   * @var mixed
   */
  private $remoteFileList = NULL;

  /**
   * Remote file latest.
   *
   * @var mixed
   */
  private $remoteFileLatest = NULL;

  /**
   * Remote file list with prefix.
   *
   * @var mixed
   */
  private $remoteFileListWithPrefix = NULL;

  /**
   * Define constants for authentication type.
   *
   * Connection settings page should refer these constant values.
   */
  const PASSWORD = 'password';
  const PRIVATE_KEY = 'private_key';

  /**
   * Initialize object creation.
   */
  public function __construct($connSettings) {

    try {
      // Set private variables from connection settings.
      $this->setAuthType($connSettings["authentication_type"]);
      $this->setHost($connSettings["host"]);
      $this->setPort($connSettings["port"]);
      $this->setRemoteDirectory($connSettings["remote_directory"]);
      $this->setLocalDirectory($connSettings["local_directory"]);
      $this->setRemoteFilePrefix($connSettings["remote_file_prefix"]);
      $this->setDownloadType($connSettings["download_type"]);

      // Create Credentials object.
      module_load_include('inc', 'file_transfer_wrapper', 'includes/FtwCredentials');
      $credsObj = new FtwCredentials($connSettings);
      $this->connect($credsObj);

      return $this;
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * Creates connection resource.
   *
   * @param \ftw\FtwCredentials $credsObj
   *   Object of type FtwCredentials.
   */
  private function connect(FtwCredentials $credsObj) {

    // Initialize $remoteResource with appropriate authentication type.
    switch ($this->authType) {
      case 'password':
        $credsObj = $credsObj->withPassword($credsObj->getUsername(), $credsObj->getPassword());
        $key = $credsObj->getPassword();
        break;

      case 'private_key':
        $credsObj = $credsObj->withPrivateKey($credsObj->getUsername(), $credsObj->getPrivateKey());
        $key = new RSA();
        if ($credsObj->getPrivateKeyPassphrase() !== NULL) {
          $key->setPassword($credsObj->getPrivateKeyPassphrase());
        }
        $key->loadKey($credsObj->getPrivateKey());
        break;
    }

    $sftp = new SFTP($this->getHost(), $this->getPort());

    if (!$sftp->login($credsObj->getUsername(), $key)) {
      throw new Exception("Login failed!");
    }

    // Change directory to the specified path.
    if (!$sftp->is_dir($this->getRemoteDirectory())) {
      throw new Exception("Configured Remote Directory does not exist!");
    }
    else {
      $sftp->chdir($this->getRemoteDirectory());
    }

    $this->setRemoteResource($sftp);
  }

  /**
   * Sets the authentication type.
   *
   * @param string $authType
   *   Authentication type.
   */
  public function setAuthType($authType) {
    $this->authType = $authType;
  }

  /**
   * Gets the set authentication type.
   *
   * @return string|null
   *   Returns authentication type.
   */
  public function getAuthType() {
    return $this->authType;
  }

  /**
   * Sets the host name.
   *
   * @param string $host
   *   Host.
   */
  public function setHost($host) {
    $this->host = $host;
  }

  /**
   * Gets the set host name.
   *
   * @return string|null
   *   Returns host.
   */
  public function getHost() {
    return $this->host;
  }

  /**
   * Sets the port.
   *
   * @param string $port
   *   Port.
   */
  public function setPort($port) {
    if (strlen($port) < 1) {
      $this->port = 22;
    }
    else {
      $this->port = $port;
    }
  }

  /**
   * Gets the port.
   *
   * @return string|null
   *   Returns port.
   */
  public function getPort() {
    return $this->port;
  }

  /**
   * Sets the remote directory.
   *
   * @param string $remoteDir
   *   Remote directory path.
   *
   * @throws Exception
   *   Remote Directory not defined!
   */
  public function setRemoteDirectory($remoteDir) {
    if (strlen($remoteDir) < 1) {
      throw new Exception("Remote directory not defined!");
    }
    $this->remoteDir = $remoteDir;
  }

  /**
   * Gets the remote directory.
   *
   * @return string|null
   *   Returns remote directory path.
   */
  public function getRemoteDirectory() {
    return $this->remoteDir;
  }

  /**
   * Sets the local directory.
   *
   * @param string $localDir
   *   Local directory path.
   *
   * @throws Exception
   *   Local Directory not defined!
   */
  public function setLocalDirectory($localDir) {
    if (strlen($localDir) < 1) {
      throw new Exception("Local directory not defined!");
    }

    // Create directory if not exist!
    $privateDir = drupal_realpath("private://") . '/' . $localDir;
    if (!file_exists($privateDir) && !is_dir($privateDir)) {
      try {
        mkdir($privateDir);
      }
      catch (Exception $e) {
        throw $e;
      }
    }

    $this->localDir = $privateDir;
  }

  /**
   * Gets the local directory.
   *
   * @return string|null
   *   Returns local directory path.
   */
  public function getLocalDirectory() {
    return $this->localDir;
  }

  /**
   * Sets Remote File Server resource.
   *
   * @param mixed $remoteResource
   *   Remote resource.
   *
   * @throws Exception
   *   File server reource not initialized.
   */
  public function setRemoteResource($remoteResource) {

    if ($remoteResource === NULL) {
      throw new Exception("File server reource not initialized!");
    }

    $this->remoteResource = $remoteResource;
  }

  /**
   * Gets Remote File Server resource.
   *
   * @return mixed
   *   Returns remote resource.
   */
  public function getRemoteResource() {
    return $this->remoteResource;
  }

  /**
   * Sets remote file prefix.
   *
   * @throws Exception
   *   Remote file prefix not been configured.
   */
  public function setRemoteFilePrefix($remoteFilePrefix) {

    if ($remoteFilePrefix === NULL) {
      throw new Exception("Remote file prefix not been configured!");
    }

    $this->remoteFilePrefix = $remoteFilePrefix;
  }

  /**
   * Gets remote file prefix.
   *
   * @return mixed
   *   Returns remote file prefix.
   */
  public function getRemoteFilePrefix() {
    return $this->remoteFilePrefix;
  }

  /**
   * Sets remote file download type.
   *
   * @param mixed $downloadType
   *   Download type(all|latest).
   *
   * @throws Exception
   *   Remote file download type not been configured.
   */
  public function setDownloadType($downloadType) {

    if ($downloadType === NULL) {
      throw new Exception("Remote file download type not been configured!");
    }

    $this->downloadType = $downloadType;
  }

  /**
   * Gets remote file download type.
   *
   * @return mixed
   *   Returns download type.
   */
  public function getDownloadType() {
    if (empty($this->downloadType)) {
      throw new Exception("Download type is not defined!");
    }
    return $this->downloadType;
  }

  /**
   * Sets remote file list.
   *
   * @param mixed $remoteFileList
   *   Remote file list.
   *
   * @throws Exception
   *   Remote file list is empty.
   */
  public function setRemoteFileList($remoteFileList) {

    if ($remoteFileList === NULL) {
      throw new Exception("Remote file list is empty!");
    }

    $this->remoteFileList = $remoteFileList;
  }

  /**
   * Gets remote file list.
   *
   * @return mixed
   *   Returns remote file list.
   */
  public function getRemoteFileList() {
    return $this->remoteFileList;
  }

  /**
   * Sets remote file list with prefix.
   *
   * @param mixed $remoteFileList
   *   Remote file list.
   *
   * @throws Exception
   *   Remote file list is empty.
   */
  public function setRemoteFileListWithPrefix($remoteFileList) {

    if ($remoteFileList === NULL) {
      throw new Exception("Remote file list is empty!");
    }

    $this->remoteFileListWithPrefix = $remoteFileList;
  }

  /**
   * Gets remote file list with prefix.
   *
   * @return mixed
   *   Returns remote file list with prefix.
   */
  public function getRemoteFileListWithPrefix() {
    return $this->remoteFileListWithPrefix;
  }

  /**
   * Sets remote file latest.
   *
   * @param mixed $remoteFileLatest
   *   Latest remote file.
   *
   * @throws Exception
   *   Remote file latest is empty.
   */
  public function setRemoteFileLatest($remoteFileLatest) {

    if ($remoteFileLatest === NULL) {
      throw new Exception("Remote file latest is empty!");
    }

    $this->remoteFileLatest = $remoteFileLatest;
  }

  /**
   * Gets remote file latest.
   *
   * @return mixed
   *   Returns latest remote file.
   */
  public function getRemoteFileLatest() {
    return $this->remoteFileLatest;
  }

  /**
   * Create directory to the remote file server.
   *
   * @param mixed $directory
   *   Directory name to be created.
   *
   * @throws Exception
   *   Remote Directory already exist.
   */
  public function createDirectory($directory) {
    try {

      $sftp = $this->getRemoteResource();

      if (!$sftp->is_dir($directory)) {
        $sftp->mkdir($directory);

        return TRUE;
      }
      else {
        throw new Exception("Remote Directory already exist!");
      }
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * Recursively delete Directory on remote file server.
   *
   * @param mixed $directory
   *   Directory name.
   *
   * @throws Exception
   *   Remote Directory doesn't exist.
   */
  public function deleteDirectory($directory) {
    try {

      $sftp = $this->getRemoteResource();

      if ($sftp->is_dir($directory)) {
        $sftp->delete($directory, TRUE);
        return TRUE;
      }
      else {
        throw new Exception("Remote Directory doesn't exist - " . $directory);
      }
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * Delete file on remote file server.
   *
   * @param mixed $directory
   *   Directory name.
   *
   * @throws Exception
   *   Remote file doesn't exist.
   */
  public function deleteFile($file) {
    try {

      $sftp = $this->getRemoteResource();

      if ($sftp->is_file($file)) {
        $sftp->delete($file, TRUE);
        return TRUE;
      }
      else {
        throw new Exception("Remote File doesn't exist - " . $file);
      }
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * Download remote files based on the Download Type.
   *
   * @param mixed $remoteFile
   *   Remote file name.
   *
   * @return mixed
   *   Returns downloaded files.
   *
   * @throws Exception
   *   Error downloading Remote File.
   */
  public function downloadFile($remoteFile = NULL) {
    try {
      $sftp = $this->getRemoteResource();
      $localPath = $this->getLocalDirectory();

      if ($remoteFile === NULL) {
        $downloadedFiles[] = $this->downloadFiles();
      }
      else {
        $localFilePath = $localPath . "/" . $remoteFile;
        if (!$sftp->get($remoteFile, $localFilePath)) {
          throw new Exception("Error downloading Remote File - " . $localFilePath);
        }

        $downloadedFiles[] = array($remoteFile, $localFilePath);
      }

      return $downloadedFiles;
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * Download files according to the download type provided.
   *
   * @return mixed
   *   Downloaded files.
   *
   * @throws Exception
   *   Error downloading remote file.
   */
  private function downloadFiles() {
    $sftp = $this->getRemoteResource();
    $localPath = $this->getLocalDirectory();
    switch ($this->getDownloadType()) {
      case 'all':
        // If list of remote files not present then generate it.
        if (empty($this->getRemoteFileListWithPrefix())) {
          $this->filesByTypeWithPrefix();
        }

        $files = $this->getRemoteFileListWithPrefix();

        foreach ($files as $file) {
          $localFilePath = $localPath . "/" . $file;
          if ($sftp->get($file, $localFilePath)) {
            $downloadedFiles[] = array($file, $localFilePath);
          }
          else {
            throw new Exception("Error downloading remote file - " . $file);
          }
        }
        break;

      case 'latest':
        $this->findLatestFile();
        $file = $this->getRemoteFileLatest();
        $localFilePath = $localPath . "/" . $file;
        if ($sftp->get($file, $localFilePath)) {
          $downloadedFiles[] = array($file, $localFilePath);
        }
        else {
          throw new Exception("Error downloading remote file - " . $file);
        }
        break;
    }
    if (count($downloadedFiles) < 1) {
      throw new Exception("Error downloading remote file!");
    }

    return $downloadedFiles;
  }

  /**
   * Find list of files by type with prefix.
   *
   * @throws Exception
   *   Remote file prefix is not defined.
   */
  public function filesByTypeWithPrefix() {
    // If list of remote files not present then generate it.
    if (empty($this->getRemoteFileList())) {
      $this->filesByType();
    }

    $files = $this->getRemoteFileList();

    if (empty($this->getRemoteFilePrefix())) {
      throw new Exception("Remote file prefix is not defined!");
    }

    $prefix = $this->getRemoteFilePrefix();
    $filesWithPrefix = preg_grep("/^$prefix.*/i", $files);
    if (count($filesWithPrefix) < 1) {
      throw new Exception("No file matches the pattern!");
    }
    $this->setRemoteFileListWithPrefix($filesWithPrefix);
  }

  /**
   * Find list of files by type.
   *
   * @throws Exception
   *   The remote folder is empty.
   */
  public function filesByType() {
    // Todo : It should come from configuration.
    $type = 'txt';

    // Get remote resource.
    $sftp = $this->getRemoteResource();

    // Get list of files on configured path.
    $files = $sftp->nlist();
    if (!count($files)) {
      throw new Exception("The remote folder is empty!");
    }

    $csvFiles = array();

    foreach ($files as $file) {
      if (!preg_match('~\w+.' . $type . '$~ism', $file)) {
        continue;
      }

      $csvFiles[] = $file;
    }

    $this->setRemoteFileList($csvFiles);
  }

  /**
   * Find latest file.
   */
  public function findLatestFile() {
    // If list of remote files not present then generate it.
    if (empty($this->getRemoteFileListWithPrefix())) {
      $this->filesByTypeWithPrefix();
    }

    $files = $this->getRemoteFileListWithPrefix();

    $mostRecent = array(
      'time' => 0,
      'file' => NULL,
    );

    // Get remote resource.
    $sftp = $this->getRemoteResource();

    foreach ($files as $file) {
      // Get the last modified time for the file.
      $time = $sftp->stat($file);

      if ($time['mtime'] > $mostRecent['time']) {
        // This file is the most recent so far.
        $mostRecent['time'] = $time['mtime'];
        $mostRecent['file'] = $file;
      }
    }

    $this->setRemoteFileLatest($mostRecent['file']);
  }

  /**
   * Upload file to remote server.
   *
   * This method expecting the file should be stored in the configured local
   * directory with the name as in $uploadFile.
   *
   * @param mixed $uploadFile
   *   File name to be uploaded.
   *
   * @return bool
   *   Returns TRUE if uploaded successfully.
   *
   * @throws Exception
   *   File exists on the remote file server.
   */
  public function uploadFile($uploadFile) {
    try {

      $sftp = $this->getRemoteResource();
      if ($sftp->file_exists($uploadFile)) {
        throw new Exception("File exists on the remote file server!");
      }
      $localPath = $this->getLocalDirectory() . "/" . $uploadFile;
      if (!$localPath) {
        throw new Exception("File do not exist on local file server!");
      }
      $sftp->put($uploadFile, file_get_contents($localPath));

      return TRUE;
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * Close remote resource connection.
   */
  public function closeConnection() {
    if (isset($this->remoteResource)) {
      unset($this->remoteResource);
    }
  }

  /**
   * Test connection method for testing remote file server.
   *
   * @throws Exception
   *   Throws exceptions at each level.
   */
  public function testConnection() {
    try {
      $dt = new DateTime();
      $testRemoteDir = "test_" . $dt->getTimestamp();

      $data_array[] = array("Hello World!");
      $prefix = "PIS_";
      $extention = ".txt";
      $delimeter = "|";

      $file_name = $this->createLocalFile($data_array, $prefix, $extention, $delimeter);
      if ($this->createDirectory($testRemoteDir)) {
        // Switch to test directory.
        $sftp = $this->getRemoteResource();
        $sftp->chdir($testRemoteDir);
        drupal_set_message("Test directory created successfully to the remote file server!");
      }
      if ($this->uploadFile($file_name)) {
        drupal_set_message("File uploaded successfully to the created Test directory! -- " . $file_name);
      }
      if ($this->downloadFile($file_name)) {
        drupal_set_message("File downloaded successfully from the created Test directory! -- " . $file_name);
      }

      // Switch to configured remote directory.
      $sftp = $this->getRemoteResource();
      $sftp->chdir('..');

      if ($this->deleteDirectory($testRemoteDir)) {
        drupal_set_message("Test directory deleted successfully from the remote file server!");
      }

      drupal_set_message("Local private directory " . $this->getLocalDirectory());
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * Create file in the local directory as configured.
   *
   * File name would be supplied prefix followed by timestamp with the supplied
   * extension.
   *
   * @param mixed $twoDDataArray
   *   Two dimensional data array.
   * @param string $prefix
   *   Prefix of the file name to be created.
   * @param string $extention
   *   Extension of the file to be created.
   * @param string $colDelimeter
   *   Column delimeter.
   *
   * @return string
   *   Returns file name.
   *
   * @throws Exception
   *   Throws exceptions at each level.
   */
  public function createLocalFile($twoDDataArray, $prefix, $extention, $colDelimeter) {

    try {
      if (empty($twoDDataArray) || !is_array($twoDDataArray)) {
        throw new Exception("Data is empty|not an array!");
      }

      $date = new DateTime();
      $local_directory = $this->getLocalDirectory();
      $file_name = $prefix . $date->getTimestamp() . $extention;
      $file_path = $local_directory . "/" . $file_name;

      if (!$file = fopen($file_path, "w")) {
        throw new Exception("Can't create file on local directory.");
      }

      foreach ($twoDDataArray as $rows) {
        $row = '';
        foreach ($rows as $column) {
          if ($row !== '') {
            $row .= $colDelimeter . $column;
          }
          else {
            $row .= $column;
          }
        }
        fwrite($file, $row);
        fwrite($file, "\r\n");
      }

      fclose($file);

      return $file_name;
    }
    catch (Exception $e) {
      throw $e;
    }
  }

}
