<?php

namespace ftw;

use Exception as Exception;

/**
 * Description of FtwSsh2Factory.
 *
 * @author SoumyaDas
 */
class FtwCredentials {

  /**
   * Authentication username.
   *
   * @var mixed
   */
  private $username = NULL;

  /**
   * Authentication password.
   *
   * @var string
   */
  private $password = NULL;

  /**
   * Private key.
   *
   * @var string
   */
  private $privateKey = NULL;

  /**
   * Private key passphrase (if needed).
   *
   * @var string
   */
  private $privateKeyPassphrase = NULL;

  /**
   * Constructor. Should not be really used. Check the static methods.
   */
  public function __construct($connSettings = NULL) {
    if ($connSettings !== NULL) {
      $this->username = $connSettings['uid'];
      $this->password = $connSettings['password'];
      $this->privateKey = $connSettings['private_key'];
    }
    return $this;
  }

  /**
   * Creates a new Credentials object for Password type authentication.
   *
   * @param mixed $username
   *   User name.
   * @param mixed $password
   *   Password.
   *
   * @return \self
   *   Returns new instance.
   */
  public static function withPassword($username, $password) {
    $instance = new self();
    $instance->setUsername($username);
    $instance->setPassword($password);

    return $instance;
  }

  /**
   * Creates a new Credentials object for Private Key type authentication.
   *
   * @param mixed $username
   *   User name.
   * @param mixed $privateKey
   *   Private key.
   * @param mixed $passphrase
   *   Pass phrase.
   *
   * @return \self
   *   Returns new instance.
   */
  public static function withPrivateKey($username, $privateKey, $passphrase = NULL) {
    $instance = new self();
    $instance->setUsername($username);
    $instance->setPrivateKey($privateKey);

    if ($passphrase !== NULL) {
      $instance->setPrivateKeyPassphrase($passphrase);
    }

    return $instance;
  }

  /**
   * Sets the authentication username.
   *
   * @param string $username
   *   User name.
   *
   * @throws Exception
   *   Username must be at least 1 character long.
   */
  public function setUsername($username) {
    if (strlen($username) < 1) {
      throw new Exception("Username must be at least 1 character long!");
    }
    $this->username = $username;
  }

  /**
   * Gets the assigned username.
   *
   * @return mixed
   *   Returns user name.
   */
  public function getUsername() {
    return $this->username;
  }

  /**
   * Sets the password.
   *
   * @param mixed $password
   *   Password.
   *
   * @throws Exception
   *   Password not defined.
   */
  public function setPassword($password) {
    if (strlen($password) < 1) {
      throw new Exception("Password not defined!");
    }
    $this->password = decrypt($password);
  }

  /**
   * Gets the assigned password.
   *
   * @return string|null
   *   Returns password.
   */
  public function getPassword() {
    return $this->password;
  }

  /**
   * Sets the private key.
   *
   * @param mixed $privateKey
   *   Private key.
   *
   * @throws Exception
   *   Private Key not defined.
   */
  public function setPrivateKey($privateKey) {
    if (strlen($privateKey) < 1) {
      throw new Exception("Private Key not defined!");
    }
    $this->privateKey = decrypt($privateKey);
  }

  /**
   * Gets the assigned private key.
   *
   * @return string|null
   *   Returns private key.
   */
  public function getPrivateKey() {
    return $this->privateKey;
  }

  /**
   * Sets the private key pass phrase.
   *
   * @param mixed $privateKeyPassphrase
   *   Pass phrase.
   *
   * @throws Exception
   *   Private Key pass phrase not defined.
   */
  public function setPrivateKeyPassphrase($privateKeyPassphrase) {
    if (strlen($privateKeyPassphrase) < 1) {
      throw new Exception("Private Key Passphrase not defined!");
    }

    $this->privateKeyPassphrase = decrypt($privateKeyPassphrase);
  }

  /**
   * Gets the assigned private key pass phrase.
   *
   * @return string|null
   *   Returns pass phrase.
   */
  public function getPrivateKeyPassphrase() {
    return $this->privateKeyPassphrase;
  }

}
